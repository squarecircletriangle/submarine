class String
  def requires_postrendering?
    !!(self =~ /\{\{\s*([\w]+)\s*(\|([^{}]*))?\}\}/)
  end
  
  def postrender(replacements)
    replacements = replacements.recursive_symbolize
    
    gsub(/\{\{\s*([\w]+)\s*(\|([^{}]*))?\}\}/i) do |match|
      key = $1.downcase.to_sym
      default = $3 || ""

      if replacements.has_key?(key)
        k = replacements[key] || ''
        (k == '') ? default.strip : k.strip
      else
        $&
      end
    end
  end
  
end

class Hash
  def recursive_symbolize
    self.keys.each do |k|
      ks = k.respond_to?(:to_sym) ? k.to_sym : k
      self[ks] = self.delete(k)
      self[ks].recursive_symbolize if self[ks].kind_of?(Hash)
    end
    self
  end
end